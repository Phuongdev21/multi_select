import React, { useEffect, useRef, useState } from "react";

const Animation = () => {
  const [chooseData, setChooseData] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const myRef = useRef(null);

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [myRef]);

  const handleClickOutside = (event) => {
    if (myRef.current && !myRef.current.contains(event.target)) {
      setIsOpen(false);
    }
  };

  const onChange = (value, type) => {
    let cd = [...chooseData];
    if (type === "add") {
      cd.push(value);
    } else {
      cd = cd.filter((d) => d.id !== value.id);
    }
    setChooseData(cd);
  };

  const data = [
    { id: "1", name: "Ocean" },
    { id: "2", name: "Blue" },
    { id: "3", name: "Purple" },
    { id: "4", name: "Red" },
    { id: "5", name: "Orange" },
    { id: "6", name: "Yellow" },
    { id: "7", name: "Green" },
    { id: "8", name: "Forest" },
    { id: "9", name: "Slate" },
    { id: "10", name: "Silver" },
  ];

  return (
    <div>
      <h3>Multi_Select</h3>

      {/* <select Onchange={handleOnchange}>
        {data.map((item, key) => (
          <option key={key}>{item.name}</option>
        ))}
      </select> */}
      {/* <input
        type="text"
        onFocus={() => {
          setIsOpen(true);
        }}
        ref={myRef}
      /> */}
      <div
        className="input-div"
        type="text"
        onMouseDown={() => {
          setIsOpen(true);
        }}
        ref={myRef}
      >
        {/* {chooseData.map((data,value)=>{


          }}, */}
        <span className="clear" onClick={() => setChooseData([])}>
          Clear
        </span>
        {chooseData.map((data, key) => (
          <span className="chooseitem" key={key}>
            {data.name}
            <span
              className="x"
              style={{ cursor: "pointer" }}
              onClick={() => {
                onChange(data, "delete");
              }}
            >
              x
            </span>
          </span>
        ))}
        {isOpen && (
          <div className="list">
            {data
              .filter(
                (d) => !chooseData.map((cd) => cd.id).includes(d.id) // TRa ve mang chua id khong nam trong mang tren//chooseData.map((cd) => cd.id)  Lay id da duoc chon
              )
              .map((item, key) => (
                <div
                  style={{ cursor: "pointer" }}
                  key={key}
                  onClick={() => {
                    onChange(item, "add");
                  }}
                >
                  {item.name}
                </div>
              ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default Animation;
